//
//  ModelPickerView.swift
//  ModelPickerApp
//
//  
//

import SwiftUI


struct ModelPickerView: View {
    @Binding var selectedModel: Model?
    @Binding var isPlacementEnabled: Bool
    var models: [Model]
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 30) {
                ForEach(0 ..< self.models.count) {
                    index in
                    Button(action: {
                        print("DEBUG: selected model with name \(self.models[index].modelName)")
                        self.selectedModel = self.models[index]
                        self.isPlacementEnabled = true
                    }, label: {
                        Image(uiImage: self.models[index].image)
                            .resizable()
                            .frame(height: 80)
                            .aspectRatio(1 / 1, contentMode: .fit)
                            .background(Color.white)
                            .cornerRadius(12)
                    }).buttonStyle(PlainButtonStyle())
                }
            }
        }
        .padding(20)
        .background(Color.black.opacity(0.5))
    }
}
