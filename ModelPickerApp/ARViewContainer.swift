//
//  ARViewContainer.swift
//  ModelPickerApp
//
//  
//

import RealityKit
import SwiftUI
import ARKit


struct ARViewContainer: UIViewRepresentable {
    
    @Binding var modelConfirmedForPlacement:Model?
    
    func makeUIView(context: Context) -> ARView {
        let arView = CustomARView(frame: .zero)
//            ARView(frame: .zero)
        

        return arView
    }
    
    func updateUIView(_ uiView: ARView, context: Context) {
            if let model = self.modelConfirmedForPlacement{
                if let modelEntity = model.modelEntity{
                    print("DEBUG: adding model to scene \(model.modelName)")
                    let anchorEntity = AnchorEntity(plane: .any)
                    anchorEntity.addChild(modelEntity.clone(recursive: true))
                    uiView.scene.addAnchor(anchorEntity)
                }
                else{
                    print("DEBUG: Unable to load modelEntity for \(model.modelName)")
                }
            }
            
            DispatchQueue.main.async {
                self.modelConfirmedForPlacement = nil
            }
        
    }
}
