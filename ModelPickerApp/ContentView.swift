//
//  ContentView.swift
//  ModelPickerApp
//
//
//

import RealityKit
import SwiftUI
import ARKit
import FocusEntity


struct ContentView: View {
    @State private var modelConfirmedForPlacement: Model?
    @State private var selectedModel: Model?
    @State private var isPlacementEnabled = false
    
    private var models: [Model] = {
        let filemanager = FileManager.default
        
        guard let path = Bundle.main.resourcePath, let files = try? filemanager.contentsOfDirectory(atPath: path) else {
            return []
        }
        var availableModels: [Model] = []
        for filename in files where filename.hasSuffix("usdz") {
            let modelName = filename.replacingOccurrences(of: ".usdz", with: "")
            let model = Model(modelName: modelName)
            
            availableModels.append(model)
        }
        return availableModels
    }()

    var body: some View {
        ZStack(alignment: .bottom) {
            ARViewContainer(modelConfirmedForPlacement: self.$modelConfirmedForPlacement)
            HStack {
                if self.isPlacementEnabled {
                    PlacementButtonsView(selectedModel: self.$selectedModel, isPlacementEnabled: $isPlacementEnabled, modelConfirmedForPlacement: self.$modelConfirmedForPlacement)
                }
                else {
                    ModelPickerView(selectedModel: self.$selectedModel, isPlacementEnabled: $isPlacementEnabled, models: models)
                }
            }
        }
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
