//
//  PlacementButtonsView.swift
//  ModelPickerApp
//
//  
//

import SwiftUI

struct PlacementButtonsView: View {
    @Binding var selectedModel: Model?
    @Binding var isPlacementEnabled: Bool
    @Binding var modelConfirmedForPlacement: Model?
    var body: some View {
        Button(action: {
            print("DEBUG:Cancel model canceled")
            self.resetPlacementParameters()
        }, label: {
            Image(systemName: "xmark")
                .frame(width: 30, height: 30)
                .font(.title)
                .background(Color.white.opacity(0.75))
                .cornerRadius(30)
                .padding(20)
        })
        Button(action: {
            print("DEBUG:Model placement confirmed")
            self.modelConfirmedForPlacement = self.selectedModel
            self.resetPlacementParameters()
        }, label: {
            Image(systemName: "checkmark")
                .frame(width: 30, height: 30)
                .font(.title)
                .background(Color.white.opacity(0.75))
                .cornerRadius(30)
                .padding(20)
        })
    }

    func resetPlacementParameters() {
        self.isPlacementEnabled = false
        self.selectedModel = nil
    }
}
