//
//  Model.swift
//  ModelPickerApp
//
// 
//

import Combine
import RealityKit
import UIKit

class Model{
    
    var modelName: String
    var image: UIImage
    var modelEntity: ModelEntity?
    
    private var cancelable: AnyCancellable?
    
    init(modelName: String) {
        self.modelName = modelName
        self.image = UIImage(named: modelName)!
        
        let filename = modelName + ".usdz"
        self.cancelable = ModelEntity.loadModelAsync(named: filename)
            .sink(receiveCompletion: {
                _ in
                print("DEBUG: Unable to load modelEntity for modelname \(self.modelName)")
            }, receiveValue: { modelEntity in
                self.modelEntity = modelEntity
                print("DEBUG: Successfully load modelEntity for modelName: \(self.modelName)")
            })
    }
}
